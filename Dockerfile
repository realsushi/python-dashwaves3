# dev dockerfile
# 1. Base image
FROM python:3.10-slim

WORKDIR /app

# here we create a new user
# note how the commands are using &&
# this helps with caching
RUN useradd -m -r user && \
    chown user /app

# 2. Copy requirements.txt
COPY requirements.txt /app

# 3. Install dependencies
RUN python -m pip install --upgrade pip --user
RUN pip install -r requirements.txt
USER user

# copy files
COPY src/ /app
#COPY src /app

CMD ["python","main.py"]
