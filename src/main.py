"""
Create app with db and init flask-login.
"""

# dependencies
import os
from flask import Flask
from flask_login import LoginManager, UserMixin
from dash import Dash, html
import dash_bootstrap_components as dbc



# internal modules
from settings import config
from views import dashboard, error, authentication
from data import binancedata as data
import routes

# exposing the 2 instances
# Exposing the Flask Server to enable configuring it for logging in
server = Flask(__name__)
app = Dash(__name__, server=server, title="crypto dashboard", external_stylesheets=[dbc.themes.SKETCHY, config.fontawesome])  # server = server


###############################################################################
#                            RUN MAIN                                         #
###############################################################################


app.layout = routes.layout

# "complete" layout
app.validation_layout = html.Div([
    routes.layout,
    data.layout,
    authentication.layout,
    dashboard.layout,
    error.layout,
])


# Updating the Flask Server configuration with Secret Key to encrypt the user session cookie
server.config.update(SECRET_KEY=os.getenv('SECRET_KEY'))


# Setup the LoginManager for the server
# Login manager object will be used to login / logout users
login_manager = LoginManager()
login_manager.init_app(server)  # massive bummer here AttributeError: 'Dash' object has no attribute 'after_request'
login_manager.login_view = '/auth'  # TODO: verify if there is a conflict with settings - so far so good!


# Create User class with UserMixin
class User(UserMixin):
    """
    minimal User class with id property. Is mandatory with Flask-login module
    nb: base has been removed, but its usage is still unclear if it was just typing or more
    """
    def __init__(self, username):
        self.id = username


# callback to reload the user object
@login_manager.user_loader
def load_user(username):
    """
    This function loads the user by user id. Typically this looks up the user from a user database.
    WIP module - should validate token
    So we'll simply return a User object with the passed in username.
    """
    return User(username)


if __name__ == "__main__":
    app.run_server(debug=config.debug, host=config.host, port=config.port)
