"""
Convenients function which valid our data.
"""

from typing import List
import pandas as pd


def valid_columns(
    data: pd.DataFrame, columns: List[str] = ["Date", "Open", "Close", "High", "Low"]
):
    """
    This function ensure that Date, Open, Close, High, Low exists.
    """

    for col in columns:
        if col not in data.columns:
            raise ValueError(
                f"""Data provided doesn't have the minimal columns.
                            column {col} doesn't inside data columns."""
            )
    return data
