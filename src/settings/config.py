import os
import logging
import re

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

## App settings
name = "Crypto dashboard"

host = "0.0.0.0"

port = int(os.environ.get("PORT", 5001))

debug = True

contacts = "https://www.linkedin.com/in/etienne-leroy-dev42"

code = "https://gitlab.com/realsushi/python-dashwaves3/"

tutorial = "https://gitlab.com/realsushi/python-dashwaves3/README.md"

fontawesome = (
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
)

## File system
root = os.path.dirname(os.path.dirname(__file__)) + "/src"

symbols_to_check = ['ETH', 'BTC', 'USDT']

database_relative_path = 'sqlite:///database/Users.db' # relative to app.py

database_absolute_path = 'sqlite:///src/database/Users.db'

## routes name

login_url = '/auth'
dashboard_url = '/dashboard'

## authenticate

password_regex = re.compile('^(?=\S{6,20}$)(?=.*?\d)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^A-Za-z\s0-9])')
error_password = '''The password needs to contain:
Min length is 6 and max length is 20, 
at least include a digit number, an upcase, 
a lowcase letter and a special characters.'''