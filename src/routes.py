"""
Creates the routes.
depends on auth
"""

from dash import html, dcc, Input, Output, callback
import dash_bootstrap_components as dbc
from flask_login import current_user


from views import authentication, dashboard, error
from components import navbar
from data import binancedata as data
from settings import config


layout = dbc.Container(
    fluid=True,
    children=[
        dcc.Location(id="url", refresh=False),
        #html.H1(config.name, id="nav-pills"),
        navbar.layout,
        html.Br(),
        html.Br(),
        html.Br(),
        html.Div(id="page-content"),
    ],
)

@callback(Output("page-content", "children"), 
              Output('log-out-button', 'hidden'), 
              Input("url", "pathname"))
def display_page(pathname):
    """
    the actual router
    pathname is defined into auth
    """
    auth_enforce_url = False # TODO fix this 
    if auth_enforce_url:
        if not current_user.is_authenticated:
            return authentication.layout, True
        elif pathname == config.login_url:
            if current_user.is_authenticated:
                return [dashboard.layout, data.layout], False
            return authentication.layout, True
        elif pathname == config.dashboard_url:
            return [dashboard.layout, data.layout], False
        else:
            return error.layout, True

    else:
        if not current_user.is_authenticated:
            # return authentication.layout, True
            return [dashboard.layout, data.layout], False
        elif pathname == config.login_url:
            # if current_user.is_authenticated:
            return [dashboard.layout, data.layout], False
            return authentication.layout, True
        elif pathname == config.dashboard_url:
            return [dashboard.layout, data.layout], False
        else:
            return error.layout, True
        # """
    '''
    BREAKTHROUGH AUTH for dev purpose
    comment if prod
    '''


