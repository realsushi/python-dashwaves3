""" Creates the dashboard view - with side panel, navbar, plotarea with charts, footer """

# dependencies
import dash_bootstrap_components as dbc
import pandas as pd
from dash import dcc, html, Input, Output, State, callback, no_update, callback_context

# internal modules
from plots import plotting
from data import binancedata as data
from constants import KLINE_INTERVALS
#wip
from components.sidepanel import main_panel, second_panel
#from components.plotarea import current_chart, grid_chart, performance_chart
#from footer import footbar

# Plot
candlestick_plot = plotting.plot_candlestick()

klines_dropdown = dbc.Form([
    dbc.Col(md=3, children=[
        "Intervals kline",
        dcc.Dropdown(
            id="klines-dropdown",
            options=[
                {"label": key, "value": key}
                for key in KLINE_INTERVALS
            ],
            clearable=False,
            value='1 day',
        )]),
    ]
)

# Input
inputs = dbc.Form(
    [
        html.H4("Select Crypto"),
        dcc.Dropdown(
            id="crypto-dropdown",
            options=[
                {"label": key, "value": key}
                for key in data.data_binance.available_coins
            ],
            value="ETHUSDT",
        ),
    ]
)


#########################################################################
# TEMPLATE
#########################################################################


layout = dbc.Row(
    [
        # input + panel
        dbc.Col(
            md=3,
            children=[
                inputs,
                html.Br(),
                html.Br(),
                html.Br(),
                dbc.Tabs(
                    id="context-panel",
                    className="nav nav-pills",
                    children=[
                        dbc.Tab(main_panel,label="INFO"),
                        dbc.Tab(second_panel,label="debug"),
                    ],
                ),
                html.Div(id="tabs-content"),
            ],
        ),
        # plots
        dbc.Col(
            md=9,
            children=[
                dbc.Col(
                    html.H4("Candlestick charts for crypto using Binance API"),
                    width={"size": 6, "offset": 3},
                ),
                dbc.Tabs(
                    className="nav nav-pills",
                    children=[
                        dbc.Tab(
                            children=[klines_dropdown,
                            dcc.Graph(id="plot-candlestick", figure=candlestick_plot)],
                            label="graph",
                        ),
                        dbc.Tab(dcc.Graph(id="plot-analysis"), label="analysis"),
                        dbc.Tab(dcc.Graph(id="plot-automation"), label="automation"),
                    ],
                ),
            ],
        ),
    ]
)



####################################################################
# CALLBACK
####################################################################


@callback(
    Output("plot-candlestick", "figure"), Input("crypto-currency-prices", "data")
)
def plot_graph_callback(data_prices):
    """ func to plot candlesticks """
    if data_prices is None:
        return no_update
    data_prices = pd.DataFrame(data_prices)
    return plotting.plot_candlestick(data_prices)
