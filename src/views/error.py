# Not found
from dash import html
import base64

image_filename = 'assets/error-ibm.png' # replace with your own image
encoded_image = base64.b64encode(open(image_filename, 'rb').read())

layout = html.Div([
    html.Div([
            html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()),style={'height':'15%', 'width':'15%', 'margin':25}),
            html.H3('Oups! Page not found', style={'fontSize': 24}),
            html.P('The page you are looking for doesn\'t exist.'),
            html.A('Go back', href='/'),
    ], style={'margin':25, 'textAlign': 'center'}),
])  