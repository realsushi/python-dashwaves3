"""
authentication page for login/register users
Need code split because too long
V1 localdb (phasing out, deprecated)
V2 with flask auth server (relying on state) (WIP)
"""
# dependency
import os
from dash import dcc, html, Input, Output, State, callback, no_update, callback_context
import dash_bootstrap_components as dbc
import base64
from flask import Flask
from flask_login import login_user, LoginManager, UserMixin, logout_user, current_user
#from werkzeug.security import check_password_hash
#from email_validator import validate_email, EmailNotValidError

# local modules
from services.user import register_user, get_token, get_token2, add_user2, existing_rows_from_email2, existing_rows_from_username2, auth_user2
from settings import config

server = Flask(__name__)

image_filename = 'assets/logo_autocoin.png' # replace with your own image
encoded_image = base64.b64encode(open(image_filename, 'rb').read())

# Updating the Flask Server configuration with Secret Key to encrypt the user session cookie
server.config.update(SECRET_KEY=os.getenv('SECRET_KEY'))

# Login manager object will be used to login / logout users
login_manager = LoginManager()
login_manager.init_app(server)
login_manager.login_view = '/login'


# User data model. It has to have at least self.id as a minimum
class User(UserMixin):
    """
    minimal User class with id property. Is mandatory with Flask-login module
    """
    def __init__(self, username):
        self.id = username


@ login_manager.user_loader
def load_user(username):
    """
    This function loads the user by user id. Typically this looks up the user from a user database.
    So we'll simply return a User object with the passed in /email.
    """
    return User(username)


def auth_page():
    """layout function as html code"""
    login_page = dbc.Container(id='auth-login', children=[
        dbc.FormFloating(
            [
                dbc.Input(id='email-box', type="text", placeholder="Enter your email", n_submit=0),
                dbc.Label("email"),
                dbc.FormFeedback("Valid email address", type="valid"),
                dbc.FormFeedback(
                    "Sorry, the email or the password is incorrect",
                    type="invalid",
                ),
            ]
        ),
        html.Br(),
        dbc.FormFloating(
            [
                dbc.Input(id='password-box', type="password", placeholder="Enter your password", n_submit=0),
                dbc.Label("Password"),
            ]
        ),
        html.Br(),
        html.Button(
            children='Login',
            n_clicks=0,
            type='submit',
            id='login-button',
            className='btn btn-primary d-grid gap-2 col-6 mx-auto'
        ),
        html.Br(),
        html.Button(
            children='Create an account',
            n_clicks=0,
            type='submit',
            id='create-page-button',
            className='btn btn-primary d-grid gap-2 col-6 mx-auto'
        ),
        dcc.Location(id="login-url", refresh=True)
    ], style={'display': 'block'})

    create_page = dbc.Container(id='auth-create', children=[

        dbc.FormFloating(
            [
                dbc.Input(id='email-create-box', type="email", placeholder="example@internet.com", n_submit=0, n_blur=0, required=True),
                dbc.Label("Email address"),
                dbc.FormFeedback("Valid email address", type="valid"),
                dbc.FormFeedback(
                    id='auth-create-email-error',
                    type="invalid",
                ),
            ]
        ),
        html.Br(),
        dbc.FormFloating(
            [
                dbc.Input(id='username-create-box', type="text", placeholder="Enter your username", n_submit=0, n_blur=0, required=True),
                dbc.Label("Username"),
                dbc.FormFeedback("Valid username", type="valid"),
                dbc.FormFeedback(
                    "This username already exists",
                    type="invalid",
                ),
            ]
        ),
        html.Br(),
        dbc.FormFloating(
            [
                dbc.Input(id='password-create-box', type="password", placeholder="Enter your password", n_submit=0, n_blur=0, required=True),
                dbc.Label("Password"),
                dbc.FormFeedback("Valid password", type="valid"),
                dbc.FormFeedback(
                    config.error_password,
                    type="invalid",
                ),
            ]
        ),
        html.Br(),
        html.Button(
            children='Register',
            n_clicks=0,
            type='submit',
            id='register-button',
            className='btn btn-primary d-grid gap-2 col-6 mx-auto'
        ),
        html.Br(),
        html.Button(
            children='Login existing account',
            n_clicks=0,
            type='submit',
            id='sign-in-button',
            className='btn btn-primary d-grid gap-2 col-6 mx-auto'
        )
    ], style={'display': 'none'})

    return [login_page, create_page]

layout = dbc.Card([
        dbc.CardImg(src='data:image/png;base64,{}'.format(encoded_image.decode()), style={'height': 350, 'width': 615},
                    top=True, className='align-self-center'),
        dbc.CardBody(children=auth_page(), className='form-group'),
])


################################################################################
# LOGIN BUTTON CLICKED / ENTER PRESSED - REDIRECT TO PAGE1 IF LOGIN DETAILS ARE CORRECT
################################################################################


"""
html template id reference:
- input form login:      email-box | username-create-box
- input form password:      password-box | password-create-box
- input form register email:      email-create-box
- button login:             login-button
- button register:          register-button
"""


@callback(
    Output('login-button', 'n_clicks'),

    Input('register-button', 'n_clicks'),

    State('username-create-box', 'value'),
    State('password-create-box', 'value'),
    State('email-create-box', 'value'),
)
#def create(n_clicks, n_submit_username, n_submit_email, n_submit_password, email, username, password, email_valid, pathname_valid, password_valid, n_clicks_signin):
def create(n_clicks, email, username, password):
    """
    callback to register user
    working on validation codebase
    """
    # TODO hash pwd before print
    if n_clicks > 0:
        print("user_create_attempt nb ", n_clicks)
        # test data here
        # if email_valid and pathname_valid and password_valid:
        try:
            new_user = register_user(username, email, password)
            print(new_user)
            print("create user! now go login")
        except:
            print("issue with register user")
        # return n_clicks_signin + 1
    # return no_update


@callback(
    Output('login-url', 'pathname'),

    Input('login-button', 'n_clicks'),

    State('email-box', 'value'),
    State('password-box', 'value'),
)
def login_button_click(n_clicks, email, password):
    """
    callback to auth user
    uses EMAIL && PASSWORD (previously username && password)
    LOGIN seems to return 422 from backend - good news!
    """
    # TODO fix callback with flexible here
    print("login_init")
    token = ""
    if n_clicks > 0:
        try:
            token = get_token(email, password)
            print(token)
        except:
            print('error get token')
        if email == 'test@test.net' and password == 'test':
            print('bypass_auth')
            """ flask login to store auth status in state """
            user = User(email)
            login_user(user)
            return '/success', '_'
        # need another block to evaluate token
        elif token != "":
            """ simple if token exists then go """
            user = User(email)
            login_user(user)
        else:
            return '/login', 'Incorrect email or password'


@callback(Output('auth-create', 'style'),
              Output('auth-login', 'style'),
              Input('sign-in-button', 'n_clicks'),
              Input('create-page-button', 'n_clicks'),
              State('auth-create', 'style'),
              State('auth-login', 'style'))
def switch_auth_page(n_clicks_signin, n_clicks_create, style_create, style_login):
    """
    button associated callback to switch page from /login (post basicauth) to /register (post register)
    """
    ctx = callback_context

    if not ctx.triggered:
        return style_create, style_login
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == 'create-page-button':
        return {'display': 'block'}, {'display': 'none'}
    elif button_id == 'sign-in-button':
        return {'display': 'none'}, {'display': 'block'}


@callback(
              Output('username-create-box', 'required'),
              Output('password-box', 'required'),
              [Input('login-button', 'n_clicks'),
               Input('username-create-box', 'n_submit'),
               Input('password-box', 'n_submit')],
              [State('username-create-box', 'value'),
               State('password-box', 'value')]
            )
def check_inputs_login(n_clicks, n_submit_username, n_submit_password, username, password):
    """"
    input form basic sanitization
    """
    if n_clicks > 0 or n_submit_username > 0 or n_submit_password > 0:
        username_required = False
        password_required = False
        if username is None or username == '':
            username_required = True
        if password is None or password == '':
            password_required = True
        return username_required, password_required
    
    return False, False


@callback(
    Output('username-create-box', 'invalid'),
    Output('username-create-box', 'valid'),
    [Input('register-button', 'n_clicks'),
    Input('username-create-box', 'n_submit'),
    Input('username-create-box', 'n_blur')],
    State('username-create-box', 'value'),
)
def check_username_create(n_clicks, n_submit, n_blur, value):
    """
    actually the backend db does check that by itself so we should listen for return code (400,401...)
    because table was defined with unique keys
    """
    if (n_clicks > 0 or n_submit > 0 or n_blur > 0) and (value is not None and value != ''):
        if existing_rows_from_username2(username=value):
            return True, False
        return False, True
    return no_update, no_update


@callback(
    Output('password-create-box', 'invalid'),
    Output('password-create-box', 'valid'),
    [Input('create-page-button', 'n_clicks'),
    Input('password-create-box', 'n_submit'),
    Input('password-create-box', 'n_blur')],
    State('password-create-box', 'value'),
)
def check_password_create(n_clicks, n_submit, n_blur, value):
    """
    to be reviewed if still needed?
    maybe for inbound p<d from backend?
    """
    if (n_clicks > 0 or n_submit > 0 or n_blur > 0) and (value is not None and value != ''):
        if config.password_regex.match(value):
            return False, True
        return True, False
    return no_update, no_update


# Button inside Navbar
@callback(
              Output('logout-url', 'pathname'),
              Input('log-out-button', 'n_clicks'),
              State('url', 'pathname') 
            )
def logout(n_clicks, pathname):
    """the logout, pretty explicit"""
    if n_clicks > 0:
        logout_user()
        return config.login_url
    else:
        return pathname
   

