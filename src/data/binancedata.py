"""
TO REFACTOR
File where websocket are handled
"""

from dash import dcc, html, Input, Output, State, callback
import pandas as pd

from constants import KLINE_INTERVALS
from helpers.api_handler import Data


columns_klines = [
    "Date",
    "Open",
    "High",
    "Low",
    "Close",
    "Volume",
    "Close time",
    "Quote asset volume",
    "Number of trades",
    "Taker buy base asset volume",
    "Taker buy quote asset volume",
    "ignored",
]
data_binance = Data()

layout = html.Div(
    [
        dcc.Store(id="crypto-currency-prices", storage_type="memory"),
        dcc.Interval(
            id="interval-component", interval=1 * 1000, n_intervals=0  # in milliseconds
        ),
    ]
)


@callback(
    Output("crypto-currency-prices", "data"),
    Input("interval-component", "n_intervals"),
    Input("klines-dropdown", "value"),
    Input("crypto-dropdown", "value"),
)
def update_metrics(n, klines_dropdown_value, crypto_dropdown_value):
     
    historical_klines = data_binance.client.get_historical_klines(
        crypto_dropdown_value, KLINE_INTERVALS[klines_dropdown_value]["value"],
        start_str=KLINE_INTERVALS[klines_dropdown_value]['start_utc']
    )

    historical_klines = pd.DataFrame(historical_klines, columns=columns_klines)

    historical_klines["Date"] = historical_klines["Date"].apply(lambda x: pd.Timestamp(x + 60 * 60 * 1000, unit="ms"))
    return historical_klines.to_dict()
