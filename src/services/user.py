"""
user service
Refactoring the original monolith app into a multi microservices app
"""
# user service
import aiohttp
import base64
import json
# import asyncio # can be used for async calls
import requests

""" Need to rebuild these methods """
# from ...components.database.users_management import add_user, existing_rows_from_email, existing_rows_from_username
root = "http://localhost:8080/"


def basic_encode(_pwd):
    """
    basic method to obfuscate sensitive data
    """
    # Standard Base64 Encoding
    encodedBytes = base64.b64encode(_pwd.encode("utf-8"))
    encodedStr = str(encodedBytes, "utf-8")
    return encodedStr


def register_user(user, email, _pass):
    """
    REGISTER USER
    calls register api by sending sensitive data (should be hashed/crypted with a speicifc signature)
    user is registered when backend acknowledges the authenticity of the message and ask db to store user data
    it is a POST request
    should go into service for refactoring
    """
    url = "http://localhost:8080/api/v1/users"
    session = requests.session()
    _hpass = basic_encode(_pass)
    print(type(user), user, type(email), email,  type(_hpass), _hpass)
    # basic test to avoid firing for nothing
    #if user != None:
    resp = session.post(url, json={
        "username": user,
        "email": email,
        "hashed_password": _hpass
    })
    return resp


def get_token(email, _pass):
    """
    LOGIN USER
    for oauth2, when user is auth, it returns a token
    that token must be stored in state and secured (hashed/crypted)
    it is a POST request
    should go into service for refactoring
    THE REQUEST OPTIONS SHOULD MATCH EXACTLY WHAT'S IN SWAGGER
    NB: Requests auto encode the top level of dicts
    """
    url = "http://localhost:8080/api/v1/token"
    session = requests.session()
    _hpass = basic_encode(_pass)   # need decode then
    print(type(email), email,  type(_hpass), _hpass)
    params={
        'username': email,
        'password': _hpass,
        'scope': '',
        'client_id': '',
        'client_secret': '',
    }
    headers={
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    resp = session.post(
        url,
        headers=headers,
        data=params,
    )
    token = json.loads(resp.text)['token']
    if resp.status_code != 200:
        print('error: ' + str(resp.status_code))
    else:
        print('token: ' + str(resp.token))
    print('Success')
    return resp


def get_token2(email, _pass):
    """
    method for get token
    """
    url = "http://localhost:8080/api/v1/token"
    token = get_access_token(url, email, _pass)
    return token


def get_access_token(url, client_id, client_secret):
    response = requests.post(
        url,
        data={"grant_type": "client_credentials"},
        auth=(client_id, client_secret),
    )
    return response.json()["access_token"]


def del_user2(email):
    pass


def show_user2(email):
    pass


def existing_rows_from_email2():
    # a get request specifically with specific query on email
    pass


def existing_rows_from_username2():
    # a get request specifically with specific query on username
    pass


"""
template api request
"""


async def get_pkm():
    async with aiohttp.ClientSession() as session:

        pokemon_url = 'https://pokeapi.co/api/v2/pokemon/151'
        async with session.get(pokemon_url) as resp:
            pokemon = await resp.json()
            print(pokemon['name'])


async def add_user_test(user, _pass):
    async with aiohttp.ClientSession() as session:
        async with session.post("http://localhost:8080/api/v1/users",  user) as resp:
            users = await resp.json()
            print(users)


async def ping_backend1():
    async with aiohttp.ClientSession as session:
        async with session.get(root) as resp:
            ping1 = await resp.json()
            await print("ping1", ping1)
    return resp


async def ping_backend2():
    session = requests.session()
    resp = session.post(root, data={})
    await print("ping2 ", resp)
    return resp


def add_user2(user, email, _pass):
    url = "http://localhost:8080/api/v1/users"
    session = requests.session()
    #print(type(user), user, type(email), email,  type(_pass), _pass)
    resp = session.post(url, json={
        "username": user,
        "email": email,
        "hashed_password": _pass
    })
    return resp


async def auth_user2(user, _pass):
    url = "http://localhost:8080/api/v1/token"
    session = requests.session()
    resp = session.post(url, json={
        "username": user,
        "hashed_password": _pass
    })
    return resp


async def auth_user22(user, _pass):
    async with aiohttp.ClientSession() as session:
        async with session.post("http://localhost:8080/api/v1/token",  user) as resp:
            token = await resp.json()
        return token

"""
snippet from scrapingbee
    r = session.post("https://news.ycombinator.com/login", data={
        "acct": "username",
        "pw": "***not-telling***"
})
"""

"""
Geeks snippet for github test https://www.geeksforgeeks.org/authentication-using-python-requests/

# import requests module
import requests
from requests.auth import HTTPBasicAuth
  
# Making a get request with user/auth only >> cannot use email with httpbasicauth lib
response = requests.get('https://api.github.com / user, ',
            auth = HTTPBasicAuth('user', 'pass'))
  
# print request object
print(response)
"""