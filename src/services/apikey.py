# apikey service

import os
from dotenv import load_dotenv
from binance.client import Client

from settings.config import symbols_to_check

# load secret key and client id for binance
load_dotenv()

def get_symbols(symbol):
    symbols = [s1 + s2 for s1 in symbols_to_check for s2 in symbols_to_check if s1 != s2 ]
    return symbol in symbols

class Data:
    def __init__(self):
        api_key = os.environ.get("binance_api")
        api_secret = os.environ.get("binance_secret")

        self.client = Client(api_key, api_secret)

        self.available_coins = [
            symbol["symbol"] for symbol in self.client.get_all_tickers() if get_symbols(symbol["symbol"])
        ]
