""" Creates the navbar. """

# dependencies
from dash import html, dcc, Input, Output, State, callback
import dash_bootstrap_components as dbc
from dash_bootstrap_components._components.Container import Container

# internal modules
from settings import config, about


logo = "assets/error-ibm.png"

# WIP feature for SPA
search_bar = dbc.Row(
    [
        dbc.Col(dbc.Input(type="search", placeholder="Search")),
        dbc.Col(
            dbc.Button(
                "Search", color="primary", className="ms-2 btn-outline-dark", n_clicks=0
            ),
            width="auto",
        ),
    ],
    className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
    align="center",
)


layout0 = dbc.Nav(
    className="nav nav-pills",
    children=[
        dbc.Row(
            children=[
                ## logo/home
                #dbc.NavItem(html.Img(src=get_asset_url("assets/error-ibm.PNG"), height="40px")),
                dbc.Col(html.Img(src = "assets/error-ibm.png", height= "50px")),
                ## TITLE
                dbc.Col(html.H3(config.name, id="nav-pills")),
                ## about
                dbc.Col(
                    dbc.NavItem(
                        html.Div(
                            [
                                dbc.NavLink("About", id="about-popover", active=False),
                                dbc.Popover(
                                    id="about",
                                    is_open=False,
                                    target="about-popover",
                                    children=[
                                        dbc.PopoverHeader("How it works"),
                                        dbc.PopoverBody(about.txt),
                                    ],
                                ),
                            ]
                        )
                    ),
                ),
                ## links
                dbc.Col(
                    dbc.DropdownMenu(
                        label="Links",
                        nav=True,
                        children=[
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-linkedin"), "  Contacts"],
                                href=config.contacts,
                                target="_blank",
                            ),
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-github"), "  Code"],
                                href=config.code,
                                target="_blank",
                            ),
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-medium"), "  Tutorial"],
                                href=config.tutorial,
                                target="_blank",
                            ),
                        ],
                    ),
                    # search dropdown menu
                ),
            ],
        align="center",
        ),
        html.Button(
                children='log out',
                n_clicks=0,
                type='submit',
                id='log-out-button',
                className='btn btn-primary',
                hidden=True
            ),
        dcc.Location(id="logout-url", refresh=True),
    ]
)


layout1 = dbc.Navbar(
        dbc.Container(
            [
                html.A(
                    # Use row and col to control vertical alignment of logo / brand
                    dbc.Row(
                        [
                            dbc.Col(html.Img(src=logo, height="30px")),
                            dbc.Col(dbc.NavbarBrand(config.name, className="ms-2")),
                        ],
                        align="left",
                        className="g-0",
                    ),
                    href="/",
                    style={"textDecoration": "none"},
                ),
                dbc.NavLink("About", id="about-popover", active=False),
                dbc.Popover(
                    id="about",
                    is_open=False,
                    target="about-popover",
                    children=[
                        dbc.PopoverHeader("How it works"),
                        dbc.PopoverBody(about.txt),
                    ],
                ),
                dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
                dbc.DropdownMenu(
                        label="Links",
                        nav=True,
                        children=[
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-linkedin"), "  Contacts"],
                                href=config.contacts,
                                target="_blank",
                            ),
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-github"), "  Code"],
                                href=config.code,
                                target="_blank",
                            ),
                            dbc.DropdownMenuItem(
                                [html.I(className="fa fa-medium"), "  Tutorial"],
                                href=config.tutorial,
                                target="_blank",
                            ),
                        ],
                ),
                dbc.Collapse(
                    search_bar,
                    id="navbar-collapse",
                    is_open=False,
                    navbar=True,
                ),
                html.Button(
                    children='log out',
                    n_clicks=0,
                    type='submit',
                    id='log-out-button',
                    className='btn btn-primary',
                    hidden=True
                ),
                dcc.Location(id="logout-url", refresh=True),
            ]
        ),
        color="light",
)


def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


# Python functions for about navitem-popover
@callback(
    output=Output("about", "is_open"),
    inputs=[Input("about-popover", "n_clicks")],
    state=[State("about", "is_open")],
)
def about_popover(n, is_open):
    if n:
        return not is_open
    return is_open


@callback(
    output=Output("about-popover", "active"),
    inputs=[Input("about-popover", "n_clicks")],
    state=[State("about-popover", "active")],
)
def about_active(n, active):
    if n:
        return not active
    return active


layout = layout1