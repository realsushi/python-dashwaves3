"""Defines the side panel with multiple tabs - templates"""
from dash import dcc, html, Input, Output, callback
import dash_bootstrap_components as dbc

from components.binancechart import bdata1234


peak_color = "grey"
data = bdata1234
data[0] = data[0]*float(100)

main_panel = [
    html.Div(
        [
            dbc.Card(
                body=True,
                className="text-black bg-white",
                children=[
                    html.H6("Change within the open week:", style={"color": "black"}),
                    html.H3(str(data[0])+"%", style={"color": "black"}),

                    html.H6("Average in 30 days:", style={"color": peak_color}),
                    html.H3(str(data[1])+" USD", style={"color": peak_color}),

                    html.H6("Current trend:", style={"color": "black"}),
                    html.H3(str(data[2]), style={"color": "clack"}),

                    html.H6("12 months peak area:", style={"color": peak_color}),
                    html.H3(str(data[3]), style={"color": peak_color}),
                ],
            )
        ]
    )
]

second_panel = [
    html.Div(
        [
            dbc.Card(
                body=True,
                className="text-black bg-white",
                children=[
                    html.H3("Tab content 2", style={"color": "black"}),
                    html.H6("this panel is for debug only and would disappear for production version", style={"color":"grey"}),
                    html.Div(dcc.Input(id='input-on-submit-1', type='text', value='callback item1')),
                    html.Div(dcc.Input(id='input-on-submit-2', type='text', value='commented item2')),
                    html.Button('TEST ping', id='submit-val', n_clicks=0),
                    html.Div(id='container-button-basic', children='test api cmd'),
                ]
            )
        ]
    )
]
