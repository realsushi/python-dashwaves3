# put plots here

import plotly.graph_objects as go


def plot_candlestick(df=None):
    """
    Plot candlestick graph. See doc : https://plotly.com/python/candlestick-charts/
    """
    fig = go.Figure()
    if df is not None:
        fig.add_trace(
            go.Candlestick(
                x=df["Date"],
                open=df["Open"],
                high=df["High"],
                low=df["Low"],
                close=df["Close"],
            )
        )
    fig.update_xaxes(rangeslider_visible=True)
    ## set background color
    fig.update_layout(
        plot_bgcolor="white",
        width=1000,
        height=550,
        xaxis_gridwidth=1,
        uirevision="constant")

    # Dynamic ticks
    fig.update_layout(
        xaxis_tickformatstops=[
            dict(dtickrange=[None, 1000], value="%H:%M:%S.%L ms"),
            dict(dtickrange=[1000, 60000], value="%HH%M:%S s"),
            dict(dtickrange=[60000, 3600000], value="%HH%M m"),
            dict(dtickrange=[3600000, 86400000], value="%H:%M h %e. %b %Y"),
            dict(dtickrange=[86400000, 604800000], value="%e. %b %Y"),
            dict(dtickrange=[604800000, "M1"], value="%e. %b %Y"),
            dict(dtickrange=["M1", "M12"], value="%b '%y %Y"),
            dict(dtickrange=["M12", None], value="%Y Y"),
        ],
    yaxis_tickprefix="$",
    xaxis_tickangle=25
    )
    return fig
