# Constant taken from binance api. start_utc has been introduced to defined the starting point
# to display in the front dash.
KLINE_INTERVALS = {
    "1 minute": {'value': '1m', 'start_utc': '1 hour ago UTC'},
    "3 minutes": {'value': '3m', 'start_utc': '3 hours ago UTC'},
    "5 minutes": {'value': '5m', 'start_utc': '5 hours ago UTC'},
    "15 minutes": {'value': '15m', 'start_utc': '15 hours ago UTC'},
    "30 minutes": {'value': '30m', 'start_utc': '30 hours ago UTC'},
    "1 hour": {'value': '1h', 'start_utc': '60 hours ago UTC'},
    "2 hours": {'value': '2h', 'start_utc': '120 hours ago UTC'},
    "4 hours": {'value': '4h', 'start_utc': '240 hours ago UTC'},
    "6 hours": {'value': '6h', 'start_utc': '360 hours ago UTC'},
    "8 hours": {'value': '8h', 'start_utc': '480 hours ago UTC'},
    "12 hours": {'value': '12h', 'start_utc': '720 hours ago UTC'},
    "1 day": {'value': '1d', 'start_utc': '1440 hours ago UTC'},
    "3 days": {'value': '3d', 'start_utc': '4320 hours ago UTC'},
    "1 week": {'value': '1w', 'start_utc': '10080 hours ago UTC'},
    "1 month": {'value': '1M', 'start_utc': '40320 hours ago UTC'},
}