# FrontDash3
## Project overview
### Summary
This repository is a frontend microservice app to be used as a dashboard of market stock exchange, especially cryptos. It is written in Python using the framework Plotly's Dash (based on Flask).


### Features and usecases
Dash plotly frontend

- [ ] landing page

- [ ] login

- [ ] dashboard


 Plot crypto data

 Plot financial data

 UI inspired from use case

 User can set and store custom parameters

 connection with backend in a MVC like behavior

## CICD
### Docker build
Docker build works by copying all the src**/** (note the trailing slash) content into /app. As such context is coherent with docker CMD (last instruction). 
### CI is done through Gitlab-CI and Docker containers


## Dev environment
### dependancy testing stuff
testing miniforge and mamba (c++ fork of miniconda) - main reason: pure python stuff is time consuming for debug and cannot go as is into CI.
1. install mamba (interactively)
```s
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh"
bash Mambaforge-$(uname)-$(uname -m).sh
```
1. `source ~/.zshrc` (to reload $PATH)
1. create env with `mamba env create -f environment.yml` it should take 54seconds on M1air-asahi-linux
1. `mamba activate myenv`

#### update mamba env
common reason: wheel pkg not building
1. `mamba deactivate && mamba env remove -n myenv` (don't forget the name flag)
1. modify environment.yml
1. `mamba env create -f environment.yml`



### Python stack on docker-compose installation phase
```s
sudo mkdir /app && cd /app
sudo apt update && sudo apt  install docker.io docker-compose -y
git clone https://github.com/grafana/tutorial-environment.git && cd tutorial-environment
docker-compose up --build -d
```

### web IDE (aka 16k pages lazy hotfix on m1 asahi-linux)
```s
pacman -S code-server
cat ~/.config/code/config.yml && code-server
``` 

### vvv OLD vvv
### Prequisites

You will need to have the following installed locally to complete this workshop:

- `python` 3.8+   
**or**
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

If you're running Docker for Desktop for macOS or Windows, Docker Compose is already included in your installation.
Remark: Docker-Desktop 2022 is no longer an open-source/free project.

#### Conda cheatsheet
```python
conda activate myenv
conda install dash==1.16 -c miniforge
conda install environment.yaml
    `conda env update --file environment.yml`

```
Conda improves cross-computer native support, where docker might not be available.

### RUN
To start the sample application and the supporting services:

```s
docker-compose up -d
```
Hint: build and up are not doing the same thing.

#### Launch *your-micro-service* natively
`python file.py`

To run it in localhost

Make sure to be in frontdash/ and

```
python run.py
```

then go to http://0.0.0.0:5001/


## Product overview

### abstract
this product is intended to be used with a restful backend (not provided).


### FastAPI backend overview (other repo)
- [ ] API dataset
- [ ] API datasource
- [ ] API users
- [ ] store data
- [ ] db connection

### File structure:

Tree :

```
📦app
 ┣ 📂components # The components are here
 ┃ ┣ 📂database # users database 
 ┃ ┃ ┣ 📜api_handler.py # Get data from binance (will be deprecated to use fastapi backend)
 ┃ ┃ ┣ 📜users_management.py # Handle user login logout
 ┃ ┃ ┣ 📜Users.db # Users database
 ┃ ┃ ┗ 📜data.py # Get data from binance (will be deprecated to use fastapi backend)
 ┃ ┣ 📂graphs # In this folder put the graphics from plotly
 ┃ ┃ ┗ 📜graphiques.py # Here graphic ui
 ┃ ┣ 📂views # Create the pages here
 ┃ ┃ ┣ 📜error.py # Here 404 ui
 ┃ ┃ ┣ 📜authentication.py # Here auth ui
 ┃ ┃ ┣ 📜dashboard.py # Here dashboard ui
 ┃ ┃ ┣ 📜navbar.py # Here navbar ui
 ┃ ┃ ┗ 📜routes.py # Here set the routes
 ┃ ┗ 📜app.py # The app is created here
 ┣ 📂settings
 ┃ ┗ 📜config.py # Here set the config variables (port,...)
 ┗ 📜constants.py # Here put the constants
 ```

### Modules structure
run.py
app.py
index
data
graph


### Product lifecycle
run

### Auth v2(with backend) feature design
https://www.realpythonproject.com/how-to-authenticate-using-keys-basicauth-oauth-in-python/
Various methods to connect to API.

- hhtp basicauth (2010 era) limits https://stackoverflow.com/questions/14797809/python-requests-library-httpbasicauth-with-three-parameters


## Contributing guidelines
 - Follow PEP8 coding style
 - Separate dependencies between external and internal modules
 - for DASH add separator for `@callback` signature! And even more if it's grouped in an array!!! or be ready to lose time wondering why the code is silent


## test cmds backup
- docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker pytest0
- docker run -d \\n  --name gitlab-runner \\n  --restart always \\n  -v $PWD:$PWD \\n  -v /var/run/docker.sock:/var/run/docker.sock \\n  gitlab/gitlab-runner:latest

## Common issues
here we store common bugs to leverage knowledge
|issue|explanation|command|
|---|---|---|
| module not found | be sure to launch python with the correct context ie, pwd should return src and not repo. (this was fixed in dockerfile due to lacking a trailing slash to define content of a folder)| `cd src` or `COPY src/ /app` instead of COPY . .
| in case of issues first start by cleaning pycache with either  |-| `find . -name '__pycache__' -type d -prune -print -exec rm -rf '{}' \;~`       or      `find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete` |  
| flask login | - |  https://stackoverflow.com/questions/31067249/attributeerror-flask-object-has-no-attribute-login-manager-login-manager
|if the issue cannot be referenced nor solved, open a ticket on the Gitlab wuth a stack overflow reference (proving minimal search effort)|-|-|
|crash werkzeug/dash | ` 'get_current_traceback' from 'werkzeug.debug.tbtools' (/usr/local/lib/python3.8/site-packages/werkzeug/debug/tbtools.py)`| update werkzeug (dependancy of dash)
| gcc crash setuptools/ backports.zoneinfo | update python 3.8.x to 3.9/3.10 for backports gcc crash|update python in env.yml|
|socket.gaierror: [Errno -3] Temporary failure in name resolution|web or dns down|reset internet or setup dns manually|